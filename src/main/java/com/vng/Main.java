package com.vng;

import com.google.flatbuffers.FlatBufferBuilder;
import com.vng.fbs.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String args[]){
        FlatBufferBuilder builder = new FlatBufferBuilder(1024);

        // Initialize John
        int johnNameOffset = builder.createString("John");
        int johnAge = 35;
        int johnEmailOffset1 = builder.createString("john@gmail.com");
        int johnEmailOffset2 = builder.createString("john@hotmail.com");
        int johnEmails[] = {johnEmailOffset1, johnEmailOffset2};
        int johnEmailsOffset = Person.createEmailVector(builder, johnEmails);
        int johnPhone1NumerOffset = builder.createString("(959) 406-3257");
        int johnPhone2NumberOffset = builder.createString("(321) 406-1234");
        int johnPhone1Offset = Phone.createPhone(builder, johnPhone1NumerOffset, PhoneType.HOME);
        int johnPhone2Offset = Phone.createPhone(builder, johnPhone2NumberOffset, PhoneType.MOBILE);
        int johnPhones[] = {johnPhone1Offset, johnPhone2Offset};
        int johnPhonesOffset = Person.createPhoneVector(builder, johnPhones);
        int johnNote1Offset = builder.createString("john note 1");
        int johnNote2Offset = builder.createString("john note 2");
        int[] johnNotes = {johnNote1Offset, johnNote2Offset};
        int johnNotesOffset = Person.createNoteVector(builder, johnNotes);
        int john = Person.createPerson(builder, johnNameOffset, johnAge, johnEmailsOffset, Group.FAMILY, johnPhonesOffset, johnNotesOffset);

        // Initialize Mary
        int maryNameOffset = builder.createString("Mary");
        int maryAge = 35;
        int maryEmailOffset1 = builder.createString("mary@gmail.com");
        int maryEmailOffset2 = builder.createString("mary@hotmail.com");
        int maryEmails[] = {maryEmailOffset1, maryEmailOffset2};
        int maryEmailsOffset = Person.createEmailVector(builder, maryEmails);
        int maryPhone1NumerOffset = builder.createString("(222) 406-2222");
        int maryPhone2NumberOffset = builder.createString("(333) 111-3333");
        int maryPhone1Offset = Phone.createPhone(builder, maryPhone1NumerOffset, PhoneType.WORK);
        int maryPhone2Offset = Phone.createPhone(builder, maryPhone2NumberOffset, PhoneType.MOBILE);
        int maryPhones[] = {maryPhone1Offset, maryPhone2Offset};
        int maryPhonesOffset = Person.createPhoneVector(builder, maryPhones);
        int[] maryNotes = {};
        int maryNotesOffset = Person.createNoteVector(builder, maryNotes);
        int mary = Person.createPerson(builder, maryNameOffset, maryAge, maryEmailsOffset, Group.FRIEND, maryPhonesOffset, maryNotesOffset);

        // Initialize Chris
        int chrisNameOffset = builder.createString("Chris");
        int chrisAge = 35;
        int chrisEmailOffset1 = builder.createString("chris@gmail.com");
        int chrisEmails[] = {chrisEmailOffset1};
        int chrisEmailsOffset = Person.createEmailVector(builder, chrisEmails);
        int chrisPhone1NumerOffset = builder.createString("(232) 111-1233");
        int chrisPhone1Offset = Phone.createPhone(builder, chrisPhone1NumerOffset, PhoneType.MOBILE);
        int chrisPhones[] = {chrisPhone1Offset};
        int chrisPhonesOffset = Person.createPhoneVector(builder, chrisPhones);
        int chrisNote1Offset = builder.createString("chris note 1");
        int[] chrisNotes = {chrisNote1Offset};
        int chrisNotesOffset = Person.createNoteVector(builder, chrisNotes);
        int chris = Person.createPerson(builder, chrisNameOffset, chrisAge, chrisEmailsOffset, Group.COWORKER, chrisPhonesOffset, chrisNotesOffset);

        int[] people = {john, mary, chris};
        int peopleOffset = AddressBook.createPeopleVector(builder, people);
        int addressBook = AddressBook.createAddressBook(builder, peopleOffset);

        builder.finish(addressBook);
        builder.dataBuffer();

        try {
            FileOutputStream fout = new FileOutputStream("output.bin");
            byte[] data = builder.sizedByteArray();
            fout.write(data);
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            byte[] dataRead = Files.readAllBytes(Paths.get("generated.bin"));
            ByteBuffer bufRead = ByteBuffer.wrap(dataRead);
            AddressBook addressBookRead = AddressBook.getRootAsAddressBook(bufRead);
            for (int i = 0; i < addressBookRead.peopleLength(); i++) {
                System.out.println(addressBookRead.people(i).email(0));
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
